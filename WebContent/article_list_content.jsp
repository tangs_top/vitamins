﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>某某家具设计公司企业官网-模板之家</title>
    ﻿<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="telephone=no">
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="alternate icon" type="image/png" href="images/favicon.png">
<link rel='icon' href='favicon.ico' type='image/x-ico' />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" href="css/default.min.css?t=227" />
<!--[if (gte IE 9)|!(IE)]><!-->
<script type="text/javascript" src="lib/jquery/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="lib/amazeui/amazeui.ie8polyfill.min.js"></script>
<![endif]-->
<script type="text/javascript" src="lib/handlebars/handlebars.min.js"></script>
<script type="text/javascript" src="lib/iscroll/iscroll-probe.js"></script>
<script type="text/javascript" src="lib/amazeui/amazeui.min.js"></script>
<script type="text/javascript" src="lib/raty/jquery.raty.js"></script>
<script type="text/javascript" src="js/main.min.js?t=1"></script>
</head>
<body>
<jsp:include page="/head"></jsp:include>
<div>
    <header id="header1" class="header-article-list">
        <div class="article-position">
          <c:choose>
          	<c:when test="${(thisIndex-1)==0}">
          		<a  href="#">
	              <span title="没有下一篇">上一篇</span>
	              <span></span>
         		</a>
          	</c:when>
          	<c:otherwise>
          		<a  href="newPage?newClassId=${thisClassiID }&page=${thisIndex-1}">
	              <span >上一篇</span>
	              <span>${inforContentTop.informationParticularsTitle }</span>
         		</a>
          	</c:otherwise>
          </c:choose>
        </div>
        <ul>
           <!--  <li class="article-on"><a href="article_list.html">行业新闻</a></li> -->
            <c:forEach items="${inforClass}" var="infor"> 
	           <c:choose>
					<c:when test="${(infor.informationClassificationID)==thisClassiID }">
	            		<li class="article-on" jiaoDian="${infor.informationClassificationID  }">
	           		 		<a href="newsClass?nId=${infor.informationClassificationID }">${infor.informationClassificationName  }</a>
	           		 	</li>
					 </c:when>  
					<c:otherwise>
	            		<li id="jiaoDian" jiaoDian="${infor.informationClassificationID  }">
	           		 		<a href="newsClass?nId=${infor.informationClassificationID }">${infor.informationClassificationName  }</a>
	           		 	</li>
					</c:otherwise>          
	            </c:choose> 
           </c:forEach>
        </ul>
        <div class="article-more-btn">
            <c:choose>
            	<c:when test="${(thisIndex+1)==count+1}">
            		<a href="#">
		                <span title="没有下一篇">下一篇</span>
		                <span></span>
            		</a>
            	</c:when>
            	<c:otherwise>
            		<a href="newPage?newClassId=${thisClassiID }&page=${thisIndex+1}">
		                <span>下一篇</span>
		                <span>${inforContentbottom.informationParticularsTitle }</span>
            		</a>
            	</c:otherwise>
            </c:choose>
        </div>
    </header>

    <section class="article-content">
            <h4>${inforContent.informationParticularsTitle }</h4>
        <main>
            ${inforContent.informationParticularsContentS } 
        </main>
    </section>
</div>
<jsp:include page="/foot"></jsp:include>
<script type="text/javascript">
	/* $(function(){
		var newsId = ${thisClassiID };
		alert(newId);
		$("li[jiaoDian='" + newsId + "']").addClass("article-on");
	}) */
	$(function(){
		
		var reDian = ${reDian };
		if(reDian==0){
			$("#header1").attr("style","display:none;");
		}
	})
	</script>
</body>

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<header>
		<div class="header-top">
			<div class="width-center">
				<div class="header-logo ">
					<img src="${Companys.companyLogo }" alt="">
				</div>
				<div class="header-title div-inline">
					<strong>${Companys.companyName }</strong> <span>${Companys.companyWebsite }</span>
				</div>

				<div class="search-box div-inline">
					<div class="input-box">
						<input type="text" name="" placeholder="             请输入关键字">
					</div>
					<div class="search-botton"></div>
				</div>
			</div>
		</div>
		<div class="header-nav">
			<button class="am-show-sm-only am-collapsed font f-btn"
				data-am-collapse="{target: '.header-nav'}">
				Menu <i class="am-icon-bars"></i>
			</button>
			<nav>
				<ul class="header-nav-ul am-collapse am-in">
					<li class="on"><a href="home" name="index">首页</a></li>
					<li><a href="about" name="about">关于我们</a></li>
					<li><a href="products" name="show">产品展示</a></li>
					<li><a href="news" name="new">新闻资讯</a></li>
					<li>
						<a href="contactUs" name="message">联系我们</a>
						<div class="secondary-menu">

							<ul>
								<li><a href="message.jsp" class="message"></a></li>
							</ul>
						</div>
					</li>
				</ul>
			</nav>
		</div>

	</header>
	<div class="am-slider am-slider-default"
		data-am-flexslider="{playAfterPaused: 8000}">
		<ul class="am-slides">
			<c:forEach items="${slideshowList }" var="bianLi">
				<li><img src="${bianLi.slideshowImgName }" alt=""></li>
			</c:forEach>
		</ul>
	</div>

</body>
</html>
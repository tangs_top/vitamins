﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
<title>某某家具设计公司企业官网-模板之家</title> ﻿
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="telephone=no">
<meta name="renderer" content="webkit">
<meta name="viewport"
	content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="alternate icon" type="image/png" href="images/favicon.png">
<link rel='icon' href='favicon.ico' type='image/x-ico' />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" href="css/default.min.css?t=227" />
<!--[if (gte IE 9)|!(IE)]><!-->
<script type="text/javascript" src="lib/jquery/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="lib/amazeui/amazeui.ie8polyfill.min.js"></script>
<![endif]-->
<script type="text/javascript" src="lib/handlebars/handlebars.min.js"></script>
<script type="text/javascript" src="lib/iscroll/iscroll-probe.js"></script>
<script type="text/javascript" src="lib/amazeui/amazeui.min.js"></script>
<script type="text/javascript" src="lib/raty/jquery.raty.js"></script>
<script type="text/javascript" src="js/main.min.js?t=1"></script>


</head>
<body>
	<jsp:include page="/head"></jsp:include>
	<section class="index-section">
		<div>
			<span></span> <span></span>
		</div>
		<div class="index-content">
			<section class="index-content-section-first">
				<div>
					<img src="${Companys.companyIntroduceImg1 }" alt="">
				</div>
			</section>
			<section class="index-content-section-second">
				<div>
					<div class="index-auto">
						<article>公司介绍</article>
						<h6>COMPANY INTRODUCTION</h6>
						<main>${Companys.companyIntroduce1 } <a class="index-button" style="width: 120px;" href="#">查看详情 &nbsp;&nbsp;</a>	
					</div>
				</div>
			</section>
		</div>
	</section>

	<section class="index-product">
		<main>
		 <ul>
			<li class="index-active"><a id="a1"
				style="background-image: url(img/ico/icoPaoChe.jpg);" href="#"></a></li>
			<li><a id="a2" style="background-image: url(img/ico/icoChuan.jpg);"
				href="#"></a></li>
			<li><a id="a3" style="background-image: url(img/ico/icoFeiJi.jpg);"
				href="#"></a></li>
			<li><a id="a4" style="background-image: url(img/ico/icoHuoJian.jpg);"
				href="#"></a></li>
		</ul>
		</main>
		<main></main>
		<main></main>
	</section>
	<div class="copyrights">
		Collect from <a href="http://www.cssmoban.com/">网页模板</a>
	</div>
	<section class="index-margin-bottom">
		<div class="index-morecase">
			<span></span> <a href="#">MORE &#62; &#62;</a>
		</div>
		<div class="index-content">
			<div class="product-list">
				<c:forEach items="${ProductdetailsList }" var="bianLi">
					<div class="am-u-sm-6 am-u-md-6 am-u-lg-4">
						<a href="productDetail?productID=${bianLi.productID }"> 
							<img src="${bianLi.productFlatPattern }" />
						</a>
					</div>
				</c:forEach>
			</div>
		</div>
	</section>


	<section class="index-margin-bottom">
		<div class="index-morecase">
			<span></span> <a href="#">MORE &#62; &#62;</a>
		</div>
		<div class="index-content">


			<div class="new-index">
				<ul>
					<li><img src="img/zhuye/xwjx.jpg" alt=""></li>
					
					<c:forEach var="news" items="${informationparticularsList}" varStatus="index">
						<c:if test="${index.index==0 }">
							<li>
								<a href="newsDetail?informationId=${news.informationParticularsId }">
									<h3>${news.informationParticularsTitle }</h3>
									<article>${news.informationParticularsContentS }</article>
								</a>
							</li>
						</c:if>
					</c:forEach>
				</ul>
				<ul>

					<c:forEach items="${informationparticularsList }" var="bianLi" varStatus="index">
						<c:if test="${index.index!= 0}">
							<li>
								<a href="newsDetail?informationId=${bianLi.informationParticularsId }">
									<h3>${bianLi.informationParticularsTitle }</h3>
									<article>${bianLi.informationParticularsContentS }</article>
								</a>
							</li>
						</c:if>
					</c:forEach>
				</ul>
			</div>
		</div>
	</section>
	<jsp:include page="/foot"></jsp:include>
</body>
<script type="text/javascript">  

   $(function(){  
       $("#a1").hover(function(){  
    	   $("#a1").css("background-image","url(img/ico/icoPaoChes.jpg)");  
      },function(){  
    	   $("#a1").css("background-image","url(img/ico/icoPaoChe.jpg)");})  
   })  
   
   $(function(){  
       $("#a2").hover(function(){  
    	   $("#a2").css("background-image","url(img/ico/icoChuans.jpg)");  
      },function(){  
    	   $("#a2").css("background-image","url(img/ico/icoChuan.jpg)");})  
   })  
   $(function(){  

       $("#a3").hover(function(){  
    	   $("#a3").css("background-image","url(img/ico/icoFeiJis.jpg)");  
      },function(){  
    	   $("#a3").css("background-image","url(img/ico/icoFeiJi.jpg)"); })  
   })  
  $(function(){  
       $("#a4").hover(function(){  
    	   $("#a4").css("background-image","url(img/ico/icoHuoJians.jpg)");  
      },function(){  
    	   $("#a4").css("background-image","url(img/ico/icoHuoJian.jpg)"); })  
   }) 
</script>  

</html>
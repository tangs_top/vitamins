﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
<title>某某家具设计公司企业官网-模板之家</title> ﻿
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="telephone=no">
<meta name="renderer" content="webkit">
<meta name="viewport"
	content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="alternate icon" type="image/png" href="images/favicon.png">
<link rel='icon' href='favicon.ico' type='image/x-ico' />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" href="css/default.min.css?t=227" />
<!--[if (gte IE 9)|!(IE)]><!-->
<script type="text/javascript" src="lib/jquery/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="lib/amazeui/amazeui.ie8polyfill.min.js"></script>
<![endif]-->
<script type="text/javascript" src="lib/handlebars/handlebars.min.js"></script>
<script type="text/javascript" src="lib/iscroll/iscroll-probe.js"></script>
<script type="text/javascript" src="lib/amazeui/amazeui.min.js"></script>
<script type="text/javascript" src="lib/raty/jquery.raty.js"></script>
<script type="text/javascript" src="js/main.min.js?t=1"></script>
</head>
<body>
	<jsp:include page="/head"></jsp:include>
	<section class="pro-list">
		<aside class="pro-leftsidebar">
			<ul>
				<li><a>产品搜索</a>
					<ul id="pro-search">
						<!-- jquery表单提交 -->
						<!-- <li><a><input type="text" id="searchA" class="pro-search">
								<a id="searchAA" class="pro-search-btn">搜索</a></a></li> -->
						<%-- ?productID=${bianLi.productID } --%>
						<!-- -------------------------------------------------------------------- -->
						<!-- <form action="productSearch" method="post" id="click">
							<li><a><input name="productName" type="text"
									id="searchA" class="pro-search"> <a id="searchAA"
									οnclick="document:click.submit()" class="pro-search-btn">搜索</a></a></li> -->
						<!-- </form> -->
						<!-- -------------------------------------------------------------------- -->

						<form action="productSearch" method="post" id="click">
							<li><a><input name="productName" type="text"
									id="searchA" class="pro-search"></a></li> <a href="#"
								class="pro-search-btn"
								onclick="document.getElementById('click').submit();">搜索</a>
						</form>



						<!-- -------------------------------------------------------------------- -->




					</ul></li>
				<li><a>产品分类</a>
					<ul id="pro-category">
						<c:forEach items="${productClassificationList }" var="bianLi">
							<li class="" jiaoDian="${bianLi.productClassificationId }">
								<a
								href="productsClass?productClassId=${bianLi.productClassificationId }">${bianLi.productClassificationName}</a>
							</li>
							<c:if test="${(bianLi.productClassificationId)==classId }">
								<script type="text/javascript">
									
								</script>
								<span style="display: none" id="xiLie1">${bianLi.productClassificationName}</span>
							</c:if>
						</c:forEach>
					</ul></li>
			</ul>
		</aside>

		<aside class="pro-rightsidebar">
			<header>

				<p></p>
				<span id="xiLie2"></span>
			</header>
			<ul>
				<c:forEach items="${ProductdetailsList }" var="bianLi">
					<li><a href="productDetail?productID=${bianLi.productID }">
							<div class="img-box">
								<img src="${bianLi.productFlatPattern }">
								<p>${bianLi.productName }</p>
							</div>
					</a></li>
				</c:forEach>
			</ul>
			<div class="pro_list_more_pages">
				<ul>
					<%-- <c:if test="${productPage!=1}"> --%>
						<li  id="topPage1"><a href="pageP?productClassId=${classId }&page=${productPage-1}">上一页</a></li>
						<li id="topPage2" style="display: none;" title="没有上一页"><a href="#">上一页</a></li>
					<%-- </c:if> --%>
					<!-- <li class="pro-list-current"><a>1</a></li>
					 -->
					<c:forEach begin="1" end="${pageList }" varStatus="in">
							<li page="${in.index }"><a href="pageP?productClassId=${classId }&page=${in.index}" >${in.index} </a></li>
					</c:forEach>
						<li  id="bottomPage1"><a href="pageP?productClassId=${classId }&page=${productPage+1}">下一页</a></li>
						<li id="bottomPage2" style="display: none;" title="没有下一页"><a href="#">下一页</a></li>
				</ul>
			</div>


		</aside>


	</section>
	<jsp:include page="/foot"></jsp:include>
	<script type="text/javascript">
		$("#searchAA").click(
				function() {
					var search = $("#searchA").val();
					if (search == "") {
						alert("未输入搜索内容");
					} else {
						$("#searchAA").attr("href","productSearch?productName=" + search);
					}
				})
		$(function() {

			var id = ${classId};
			var xiLie = $("#xiLie1").html();
			$("#xiLie2").html(xiLie);
			$("li[jiaoDian='" + id + "']").addClass("on");
			/* $("li[jiaoDian='"+id+"}']").addClass("on");	 */
			
/* 分页部分 */
			/* 当前页 */
			var page =${productPage};
			
			$("li[page='" + page + "']").addClass("pro-list-current");
			/* 上一页 */
			if (page==1){
				$("#topPage1").css("display","none");
				$("#topPage2").css("display","");
			}
			
			/* 下一页 */
			var pageList =${pageList};
			if (page==pageList){
				$("#bottomPage1").css("display","none");
				$("#bottomPage2").css("display","");
			}
		});
	</script>
</body>
</html>
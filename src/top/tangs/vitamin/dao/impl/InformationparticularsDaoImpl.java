package top.tangs.vitamin.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import top.tangs.vitamin.entity.Informationclassification;
import top.tangs.vitamin.entity.Informationparticulars;
import top.tangs.vitamin.util.DBManager;


public class InformationparticularsDaoImpl {
	public List<Informationparticulars> getInformationList() {
		List<Informationparticulars> list = new ArrayList<Informationparticulars>();
		// 1鑾峰彇杩炴帴
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2鍒涘缓statement瀵硅薄(鎵цSQL璇彞)
			st = conn.createStatement();
			// 3銆係QL璇彞
			String sql = "SELECT * FROM informationparticulars,informationclassification WHERE informationClassificationID = InformationParticularsClassification";

			// 4.鎵цSQL璇彞
//		st.executeQuery(sql);//

			rs = st.executeQuery(sql);//

//		5.渚垮埄缁撴灉闆�;
			while (rs.next()) {
				int id = rs.getInt("informationParticularsId");
				String title = rs.getString("informationParticularsTitle");
				String content = rs.getString("informationParticularsContent");
				Date time = rs.getDate("informationParticularsTime");

				int classID = rs.getInt("informationClassificationID");
				String className = rs.getString("informationClassificationName");

				Informationparticulars informationList = new Informationparticulars(id, title, content, time, classID,
						className);
				list.add(informationList);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}
	public List<Informationparticulars> getInformationList(int nId) {
		List<Informationparticulars> list = new ArrayList<Informationparticulars>();
		// 1鑾峰彇杩炴帴
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2鍒涘缓statement瀵硅薄(鎵цSQL璇彞)
			st = conn.createStatement();
			// 3銆係QL璇彞
			String sql = "SELECT * FROM informationparticulars,informationclassification WHERE informationClassificationID = InformationParticularsClassification and informationParticularsClassification ="+nId;
			
			// 4.鎵цSQL璇彞
//		st.executeQuery(sql);//
			
			rs = st.executeQuery(sql);//
			
//		5.渚垮埄缁撴灉闆�;
			while (rs.next()) {
				int id = rs.getInt("informationParticularsId");
				String title = rs.getString("informationParticularsTitle");
				String content = rs.getString("informationParticularsContent");
				Date time = rs.getDate("informationParticularsTime");
				
				int classID = rs.getInt("informationClassificationID");
				String className = rs.getString("informationClassificationName");
				
				Informationparticulars informationList = new Informationparticulars(id, title, content, time, classID,
						className);
				list.add(informationList);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}
		
	}
	public List<Informationclassification> getInforClassList() {
		List<Informationclassification> list = new ArrayList<Informationclassification>();
		// 1鑾峰彇杩炴帴
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2鍒涘缓statement瀵硅薄(鎵цSQL璇彞)
			st = conn.createStatement();
			// 3銆係QL璇彞
			String sql = "SELECT * FROM informationclassification";
			
			// 4.鎵цSQL璇彞
//		st.executeQuery(sql);//
			
			rs = st.executeQuery(sql);//
			
//		5.渚垮埄缁撴灉闆�;
			while (rs.next()) {
				int classID = rs.getInt("informationClassificationID");
				String className = rs.getString("informationClassificationName");
				
				Informationclassification inforClass = new Informationclassification(classID,className);
				list.add(inforClass);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}
		
	}
	//详情页
	public Informationparticulars getInforDetailList(int nId) {
		Informationparticulars informationparticulars = new Informationparticulars();
		// 1鑾峰彇杩炴帴
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2鍒涘缓statement瀵硅薄(鎵цSQL璇彞)
			st = conn.createStatement();
			// 3銆係QL璇彞
			String sql = "SELECT informationParticularsId,informationParticularsTitle,informationParticularsContentS,informationParticularsClassification FROM informationparticulars where informationParticularsId ="+nId;
			
			// 4.鎵цSQL璇彞
//		st.executeQuery(sql);//
			
			rs = st.executeQuery(sql);//
			
//		5.渚垮埄缁撴灉闆�;
			while (rs.next()) {
				int id = rs.getInt("informationParticularsId");
				String title = rs.getString("informationParticularsTitle");
				String contentS = rs.getString("informationParticularsContentS");
				int classID = rs.getInt("informationParticularsClassification");
				informationparticulars = new Informationparticulars(id, title,contentS, classID);
			}
			return informationparticulars;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}
		
	}
	
	
	//index.jsp--精选新闻案例
	public List<Informationparticulars> getJingXuanList() {
		List<Informationparticulars> list = new ArrayList<Informationparticulars>();
		// 1鑾峰彇杩炴帴
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2鍒涘缓statement瀵硅薄(鎵цSQL璇彞)
			st = conn.createStatement();
			// 3銆係QL璇彞
			String sql = "SELECT * FROM informationparticulars order by rand() limit 5";

			// 4.鎵цSQL璇彞
//		st.executeQuery(sql);//

			rs = st.executeQuery(sql);//

//		5.渚垮埄缁撴灉闆�;
			while (rs.next()) {
				int id = rs.getInt("informationParticularsId");
				String title = rs.getString("informationParticularsTitle");
				String content = rs.getString("informationParticularsContent");
				int cid = rs.getInt("informationParticularsClassification");

				Informationparticulars informationList = new Informationparticulars(id, title, content,cid);
				list.add(informationList); 	
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}
//--------------------------------------------------------------------------------
//	获取新闻总条数
	/**
	 *方法名:getInforCount
	 *返回值类型:int
	 *方法描述:
	 * @param classId
	 * @return
	 *创建时间:2020年5月30日 下午8:42:08
	 *作者:Tang
	 */
	public int getInforCount(int classId) {
		int count = 0;
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			st = conn.createStatement();
			String sql = "SELECT count(*) FROM informationparticulars,informationclassification where informationClassificationID = informationParticularsClassification and informationParticularsClassification = "+classId;
			rs = st.executeQuery(sql);//
			while (rs.next()) {
				 count = rs.getInt("count(*)");
			}
			return count;
		} catch (SQLException e) {e.printStackTrace();return 0;}
		finally {DBManager.closeAll(conn, st, rs);}
	}
//	获取当前新闻
//	------------------------------------------------------------------------
	public Informationparticulars getInforThisCount(int thisCount,int classId) {
		int thisCounts = thisCount-1 ;
		Informationparticulars list = new Informationparticulars();
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			st = conn.createStatement();
			String sql = "SELECT * FROM informationparticulars,informationclassification where informationClassificationID = informationParticularsClassification and informationParticularsClassification = "+classId+" limit "+thisCounts+",1";
			rs = st.executeQuery(sql);//
			while (rs.next()) {
				int id = rs.getInt("informationParticularsId");
				String title = rs.getString("informationParticularsTitle");
				String contentS = rs.getString("informationParticularsContentS");
				int classID = rs.getInt("informationParticularsClassification");
				 list= new Informationparticulars(id,title,contentS, classID);
			}
			return list;
		} catch (SQLException e) {e.printStackTrace();return null;}
		finally {DBManager.closeAll(conn, st, rs);}
	}
<<<<<<< HEAD
	/**
	 *ytt
	 *查询资讯
	 */
	public List<Informationparticulars> getInformaationList() {
		List<Informationparticulars> ifpList = new ArrayList<Informationparticulars>();
		// 1鑾峰彇杩炴帴
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2鍒涘缓statement瀵硅薄(鎵цSQL璇彞)
			st = conn.createStatement();
			// 3銆係QL璇彞
			String sql = "SELECT * FROM informationparticulars";
			
			// 4.鎵цSQL璇彞
//		st.executeQuery(sql);//
			
			rs = st.executeQuery(sql);//
			
//		5.渚垮埄缁撴灉闆�;
			while (rs.next()) {
				int id = rs.getInt("informationParticularsId");
				String title = rs.getString("informationParticularsTitle");
				Date time = rs.getDate("informationParticularsTime");
				Informationparticulars info = new Informationparticulars(id, title, time);
				ifpList.add(info);
			}
			return ifpList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}
		
	}
	/**
	 * ytt
	 * 删除资讯
	 */
	public int delproductClassificationId(int iId) {
		return DBManager.executeUpdate("delete from informationparticulars where informationParticularsId="+iId);
	}
=======
	
	
	
	
	
>>>>>>> 8cb77bb7364116f809f0c450959ce7784b54f7c6
}





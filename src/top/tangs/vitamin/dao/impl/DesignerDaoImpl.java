package top.tangs.vitamin.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import top.tangs.vitamin.entity.Designer;
import top.tangs.vitamin.util.DBManager;

public class DesignerDaoImpl {
	public List<Designer> getDesignerList() {
		List<Designer> list = new ArrayList<Designer>();
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;

		try {
			st = conn.createStatement();
			rs = st.executeQuery("select * from designer");
			while (rs.next()) {
				
				int id = rs.getInt("designerId");
				String headPortaits = rs.getString("designerHeadPortraits");
				String naem = rs.getString("designerName");
				String position = rs.getString("designerPosition");

				Designer d = new Designer(id, headPortaits, naem, position);

				list.add(d);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}
	}
}

package top.tangs.vitamin.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import top.tangs.vitamin.entity.Company;
import top.tangs.vitamin.util.DBManager;

public class CompanyDaoImpl {
	public Company getCompanys() {
		
		Company Companys = new Company();
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery("select * from company");
			while (rs.next()) {
				String name = rs.getString("companyName");
				String website = rs.getString("companyWebsite");
				String logo = rs.getString("companyLogo");
				String hotline = rs.getString("companyHotline");
				String phone = rs.getString("companyPhone");
				String email = rs.getString("companyEmail");
				String address = rs.getString("companyAddress");
				String erWeiMa = rs.getString("companyErWeiMa");
				String lntroduce1 = rs.getString("companyIntroduce1");
				String intyoducelmg1 = rs.getString("companyIntroduceImg1");
				String introduce2 = rs.getString("companyIntroduce2");
				String introduceImg2 = rs.getString("companyIntroduceImg2");
				String culture = rs.getString("companyCulture");
				String cultureImg = rs.getString("companyCultureImg");
				  Companys = new Company(name,website,logo,hotline,phone,email,address,erWeiMa,lntroduce1,intyoducelmg1,introduce2,introduceImg2,culture,cultureImg);

//				   
			}
			return Companys;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}
	}
}

package top.tangs.vitamin.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import top.tangs.vitamin.entity.Company;
import top.tangs.vitamin.entity.Informationclassification;
import top.tangs.vitamin.entity.Productclassification;
import top.tangs.vitamin.entity.Productdetails;
import top.tangs.vitamin.util.DBManager;

public class ProductdetailsDaoImpl {
//	閺屻儴顕楅弫鐗堝祦鎼存挷鑵戦惃鍕殶閹癸拷
	public List<Productdetails> getProductdetailsList() {
		List<Productdetails> list = new ArrayList<Productdetails>();
		// 1閼惧嘲褰囨潻鐐村复
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2閸掓稑缂搒tatement鐎电钖�(閹笛嗩攽SQL鐠囶厼褰�)
			st = conn.createStatement();
			// 3閵嗕總QL鐠囶厼褰�
			String sql = "select * from productdetails where productTheCategory = 1 limit 8";

			// 4.閹笛嗩攽SQL鐠囶厼褰�
//		st.executeQuery(sql);//

			rs = st.executeQuery(sql);//

//		5.娓氬灝鍩勭紒鎾寸亯闂嗭拷;
			while (rs.next()) {
				int productID = rs.getInt("productID");
				String productName = rs.getString("productName");
				String productFlatPattern = rs.getString("productFlatPattern");

				Productdetails productdetails = new Productdetails(productID, productName, productFlatPattern);
				list.add(productdetails);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}

	public List<Productdetails> getProductclassificationList(int productId) {
		List<Productdetails> list = new ArrayList<Productdetails>();
		// 1閼惧嘲褰囨潻鐐村复
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2閸掓稑缂搒tatement鐎电钖�(閹笛嗩攽SQL鐠囶厼褰�)
			st = conn.createStatement();
			// 3閵嗕總QL鐠囶厼褰�
			String sql = "select * from productdetails  where productTheCategory =" + productId+" limit 8";

			// 4.閹笛嗩攽SQL鐠囶厼褰�
//		st.executeQuery(sql);//

			rs = st.executeQuery(sql);//

//		5.娓氬灝鍩勭紒鎾寸亯闂嗭拷;
			while (rs.next()) {
				int productID = rs.getInt("productID");
				String productName = rs.getString("productName");
				String productFlatPattern = rs.getString("productFlatPattern");
				Productdetails productdetails = new Productdetails(productID, productName, productFlatPattern);
				list.add(productdetails);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}
	public List<Productdetails> getProductSearchList(String pName) {
		List<Productdetails> list = new ArrayList<Productdetails>();
		// 1閼惧嘲褰囨潻鐐村复
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2閸掓稑缂搒tatement鐎电钖�(閹笛嗩攽SQL鐠囶厼褰�)
			st = conn.createStatement();
			// 3閵嗕總QL鐠囶厼褰�
			String sql = "select * from productdetails where productName like n'%"+pName+"%'";
			// 4.閹笛嗩攽SQL鐠囶厼褰�
//			String s="select * from productdetails where productName like '%鐏%'";
//		st.executeQuery(sql);//

			rs = st.executeQuery(sql);//

//		5.娓氬灝鍩勭紒鎾寸亯闂嗭拷;
			while (rs.next()) {
				int id = rs.getInt("productID");
				String name = rs.getString("productName");
				String flatPattern = rs.getString("productFlatPattern");
				Productdetails details = new Productdetails(id,name,flatPattern);
				list.add(details);
			}
//			while (rs.next()) {
//				int productID = rs.getInt("productID");
//				String productName = rs.getString("productName");
//				String productFlatPattern = rs.getString("productFlatPattern");
//				Productdetails productdetails = new Productdetails(productID, productName, productFlatPattern);
//				list.add(productdetails);
//			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}
	

	public List<Productclassification> getProductclassificationList() {
		List<Productclassification> list = new ArrayList<Productclassification>();
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			st = conn.createStatement();
			String sql = "SELECT * FROM productclassification";
			rs = st.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("productClassificationId");
				String name = rs.getString("productClassificationName");
				String img = rs.getString("productClassificationImg");
				Productclassification productClassificationList = new Productclassification(id, name, img);
				list.add(productClassificationList);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}

	// 璇︽儏椤甸潰
	public Productdetails getProductXQList(int pId) {
		Productdetails pList = new Productdetails(pId, null, null, pId, pId, null, null, null,null, null, null, null, null, null, null, null, null, pId, null);
		Company Companys = new Company();
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;

		try {
			st = conn.createStatement();
			String sql = "SELECT * FROM productdetails,productclassification where productID=" + pId;
			rs = st.executeQuery(sql);
			while (rs.next()) {
				int pTId = rs.getInt("productID");
				String pName = rs.getString("productName");
				String texture = rs.getString("productTexture");
				int pPrice = rs.getInt("productPrice");
				int pMPrice = rs.getInt("productMarketPrice");
				String pFlatPattern = rs.getString("productFlatPattern");
				String pImg = rs.getString("productImg");
				String pExplain = rs.getString("productExplain");
				String pDescription = rs.getString("productDetailedDescription");
				String pDrand = rs.getString("productDrand");
				String pVersion = rs.getString("productVersion");
				String pSyle = rs.getString("productStyle");
				String pKDorNot = rs.getString("productKDorNot");
				String pColorClassification = rs.getString("productColorClassification");
				String pInscribePositioning = rs.getString("productInscribePositioning");
				String pCustomDesign = rs.getString("productCustomDesign");
				String pDesignElement = rs.getString("productDesignElement");
				int pTheCategory = rs.getInt("productTheCategory");
				String cname = rs.getString("productClassificationName");
				String cimg = rs.getString("productClassificationImg");
				pList = new Productdetails(pTId, pName, texture, pPrice, pMPrice, pFlatPattern, pImg, pExplain, pDescription,pDrand, pVersion, pSyle, pKDorNot, pColorClassification, pInscribePositioning, pCustomDesign,pDesignElement, pTheCategory, cname);
				
			}
			return pList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}
	}

	
	
	//涓婚〉鍟嗗搧绮鹃�夋渚�
	public List<Productdetails> getJingXuanList() {
		List<Productdetails> list = new ArrayList<Productdetails>();
		// 1閼惧嘲褰囨潻鐐村复
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2閸掓稑缂搒tatement鐎电钖�(閹笛嗩攽SQL鐠囶厼褰�)
			st = conn.createStatement();
			// 3閵嗕總QL鐠囶厼褰�
			String sql = "select * from productdetails order by rand() limit 6";

			// 4.閹笛嗩攽SQL鐠囶厼褰�
//		st.executeQuery(sql);//

			rs = st.executeQuery(sql);//

//		5.娓氬灝鍩勭紒鎾寸亯闂嗭拷;
			while (rs.next()) {
				int productID = rs.getInt("productID");
				String productFlatPattern = rs.getString("productFlatPattern");
				Productdetails productdetails = new Productdetails(productID, productFlatPattern);
				list.add(productdetails);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}
	
	
	//鍒嗛〉鏌ヨ
	public List<Productdetails> getFenYeProductdetailsList(int page , int classId,int limitList) {
		List<Productdetails> list = new ArrayList<Productdetails>();
		// 1閼惧嘲褰囨潻鐐村复
		int tiaoShu = (page-1)*limitList;
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2閸掓稑缂搒tatement鐎电钖�(閹笛嗩攽SQL鐠囶厼褰�)
			st = conn.createStatement();
			// 3閵嗕總QL鐠囶厼褰�
			String sql = "select * from productdetails where productTheCategory = "+classId+"  limit "+tiaoShu+","+limitList;

			// 4.閹笛嗩攽SQL鐠囶厼褰�
//		st.executeQuery(sql);//

			rs = st.executeQuery(sql);//

//		5.娓氬灝鍩勭紒鎾寸亯闂嗭拷;
			while (rs.next()) {
				int productID = rs.getInt("productID");
				String productName = rs.getString("productName");
				String productFlatPattern = rs.getString("productFlatPattern");

				Productdetails productdetails = new Productdetails(productID, productName, productFlatPattern);
				list.add(productdetails);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}
	}
	
	//鎬绘暟閲�
	//鍒嗛〉鏌ヨ
		public int getProductCount(int page , int classId) {
			int count = 0;
			// 1閼惧嘲褰囨潻鐐村复
			Connection conn = DBManager.getConnection();
			Statement st = null;
			ResultSet rs = null;
			try {
				// 2閸掓稑缂搒tatement鐎电钖�(閹笛嗩攽SQL鐠囶厼褰�)
				st = conn.createStatement();
				// 3閵嗕總QL鐠囶厼褰�
				String sql = "select count(*) from productdetails where productTheCategory = "+classId;

				// 4.閹笛嗩攽SQL鐠囶厼褰�
//			st.executeQuery(sql);//

				rs = st.executeQuery(sql);//

//			5.娓氬灝鍩勭紒鎾寸亯闂嗭拷;
				while (rs.next()) {
					 count = rs.getInt("count(*)");
				}
				return count;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 0;
			} finally {
				DBManager.closeAll(conn, st, rs);
			}
		}	
	
<<<<<<< HEAD
	/*
	 * 根据分类id删除产品分类
	 * */
	public int delProductClassificationId(int pId) {
		return DBManager.executeUpdate("delete from productclassification where productClassificationId="+pId);
	}
=======
		/**
		 *方法名:getDelPCclass
		 *返回值类型:int
		 *方法描述:删除产品分类
		 * @param classId
		 * @return
		 *创建时间:2020年5月31日 上午1:06:52
		 *作者Tang
		 */
		public int getDelPCclass(int classId) {
			return DBManager.executeUpdate("DELETE FROM productclassification WHERE productClassificationId ="+classId);
		}
>>>>>>> 8cb77bb7364116f809f0c450959ce7784b54f7c6
		
		
}


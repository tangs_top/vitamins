package top.tangs.vitamin.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import top.tangs.vitamin.entity.Productdetails;
import top.tangs.vitamin.util.DBManager;

public class PageTestDaoImpl {
	public List<Productdetails> getProductdetailsPageList(int pageCount,int classId) {
		List<Productdetails> list = new ArrayList<Productdetails>();
		// 1闁兼儳鍢茶ぐ鍥ㄦ交閻愭潙澶�
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2闁告帗绋戠紓鎼抰atement閻庣數顢婇挅锟�(闁圭瑳鍡╂斀SQL閻犲浂鍘艰ぐ锟�)
			st = conn.createStatement();
			// 3闁靛棔绺絈L閻犲浂鍘艰ぐ锟�
			String sql = "select * from productdetails where productTheCategory = "+classId+" limit "+pageCount+",4";

			// 4.闁圭瑳鍡╂斀SQL閻犲浂鍘艰ぐ锟�
//		st.executeQuery(sql);//
			rs = st.executeQuery(sql);//

//		5.濞撴艾鐏濋崺鍕磼閹惧浜梻鍡嫹;
			while (rs.next()) {
				int productID = rs.getInt("productID");
				String productName = rs.getString("productName");
				String productFlatPattern = rs.getString("productFlatPattern");
				int page = pageCount;
				Productdetails productdetails = new Productdetails(productID, productName, productFlatPattern,page);
				list.add(productdetails);
				System.out.println("鏌ヨ缁撴灉"+productdetails);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}
	
	
	
	public int getProductdetailsPageCountList(int classId) {
		int count = 0;
		// 1闁兼儳鍢茶ぐ鍥ㄦ交閻愭潙澶�
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			// 2闁告帗绋戠紓鎼抰atement閻庣數顢婇挅锟�(闁圭瑳鍡╂斀SQL閻犲浂鍘艰ぐ锟�)
			st = conn.createStatement();
			// 3闁靛棔绺絈L閻犲浂鍘艰ぐ锟�
			String sql = " select count(*) from productdetails where productTheCategory = 1";

			// 4.闁圭瑳鍡╂斀SQL閻犲浂鍘艰ぐ锟�
//		st.executeQuery(sql);//
			rs = st.executeQuery(sql);//

//		5.濞撴艾鐏濋崺鍕磼閹惧浜梻鍡嫹;
			while (rs.next()) {
				count = rs.getInt("count(*)");
			}
			return count ;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		} finally {
			DBManager.closeAll(conn, st, rs);
		}

	}
	
	
	
	
	
}

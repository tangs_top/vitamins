package top.tangs.vitamin.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import top.tangs.vitamin.entity.Slideshow;
import top.tangs.vitamin.util.DBManager;
public class SlideshowDaoImpl {
	public List<Slideshow> getSlideshowList(){
		List<Slideshow> list = new ArrayList<Slideshow>();
		//1获取连接
		Connection conn = DBManager.getConnection();
		Statement st = null;
		ResultSet rs = null;
		try {
			//2创建statement对象(执行SQL语句)
			 st = conn.createStatement(); 
			//3。SQL语句
			String sql = "select * from slideshow";
			
			//4.执行SQL语句
//			st.executeQuery(sql);//
			
			rs = st.executeQuery(sql);//
			
//			5.便利结果集;
			while(rs.next()) {
				int id = rs.getInt("slideshowId");
				String name = rs.getString("slideshowImgName");
				Slideshow slideshows= new Slideshow(id,name);
				list.add(slideshows);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}finally {
			DBManager.closeAll(conn, st, rs);
		}
		
	}
}

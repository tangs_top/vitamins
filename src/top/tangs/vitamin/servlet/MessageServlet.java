package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.util.Result;
import top.tangs.vitamin.dao.impl.UserDaoImpl;
import top.tangs.vitamin.entity.UserLiuYan;

/**
 * Servlet implementation class MessageServlet
 */
public class MessageServlet extends HttpServlet {
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String tel = request.getParameter("tel");
		String email = request.getParameter("email");
		String content = request.getParameter("content");
		UserLiuYan userLiuYan = new UserLiuYan(username, tel, email, content);
		System.out.println(userLiuYan);
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		UserDaoImpl userDao = new UserDaoImpl();
		int t = userDao.addMessage(userLiuYan);
		if(t>0) {
			out.print(Result.toClient(true, "添加成功", ""));
			//返回值 
			//
		}else {
			out.print(Result.toClient(false, "留言失败","服务器异常"));
		}
		out.flush();
		out.close();
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}

package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.ProductdetailsDaoImpl;
import top.tangs.vitamin.entity.Productclassification;
import top.tangs.vitamin.entity.Productdetails;

public class ProductSearchServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// 产品表数据
		ProductdetailsDaoImpl ProductdetailsDao = new ProductdetailsDaoImpl();
		// 产品分类
		List<Productclassification> productClassificationList = ProductdetailsDao.getProductclassificationList();
		request.setAttribute("productClassificationList", productClassificationList);
		// 搜索
//		String productName = (String)(request.getParameter("productName"));
//		Object productNameX = (Object)productName;
//		System.out.println(productName);
//		boolean nameIsNull =  productNameX.equals(null);
//		System.out.println(nameIsNull);

		Object productNameX = request.getParameter("productName");
		boolean nameIsNull = productNameX == null;
		String productName =(String)productNameX;

//商品查询		

		// 搜索
		if (nameIsNull) {
//			查询所有商品
			List<Productdetails> ProductdetailsList = ProductdetailsDao.getProductdetailsList();
			request.setAttribute("ProductdetailsList", ProductdetailsList);
		} else {
			// 搜索商品
			List<Productdetails> ProductdetailsList = ProductdetailsDao.getProductSearchList(productName);
			request.setAttribute("ProductdetailsList", ProductdetailsList);
		}
		request.getRequestDispatcher("productlist.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}

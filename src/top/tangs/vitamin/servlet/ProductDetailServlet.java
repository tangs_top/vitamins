package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.ProductdetailsDaoImpl;
import top.tangs.vitamin.entity.Productclassification;
import top.tangs.vitamin.entity.Productdetails;

/**
 * Servlet implementation class ProductDetailServlet
 */
public class ProductDetailServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ProductdetailsDaoImpl ProductdetailsDao = new ProductdetailsDaoImpl();
		// ��Ʒ����
		List<Productclassification> productClassificationList = ProductdetailsDao.getProductclassificationList();
		request.setAttribute("productClassificationList", productClassificationList);
		
		Object pIdX = request.getParameter("productID");
		boolean pIdIsNull = pIdX == null;
		int pId = Integer.parseInt((String) pIdX);
		if (!pIdIsNull) {
			ProductdetailsDaoImpl productdetailsDao = new ProductdetailsDaoImpl();
			Productdetails productdetailsXQList = productdetailsDao.getProductXQList(pId);
			request.setAttribute("pXQList", productdetailsXQList);
		}

		request.getRequestDispatcher("productdetails.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}

package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.InformationparticularsDaoImpl;
import top.tangs.vitamin.dao.impl.ProductdetailsDaoImpl;
import top.tangs.vitamin.entity.Informationparticulars;
import top.tangs.vitamin.entity.Productdetails;

public class HomeServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 商品精选案例
		ProductdetailsDaoImpl ProductdetailsDao = new ProductdetailsDaoImpl();
		List<Productdetails> ProductdetailsList = ProductdetailsDao.getJingXuanList();
		request.setAttribute("ProductdetailsList", ProductdetailsList);
		
		//新闻精选案例
		InformationparticularsDaoImpl informationparticularsDao = new InformationparticularsDaoImpl();
		List<Informationparticulars> informationparticularsList = informationparticularsDao.getJingXuanList();
		request.setAttribute("informationparticularsList", informationparticularsList);
		System.out.println(informationparticularsList);
		
		request.getRequestDispatcher("indexs.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}

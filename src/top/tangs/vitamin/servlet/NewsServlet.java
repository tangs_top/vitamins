package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.InformationparticularsDaoImpl;
import top.tangs.vitamin.entity.Informationclassification;
import top.tangs.vitamin.entity.Informationparticulars;

/**
 * Servlet implementation class NewsServlet
 */
public class NewsServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		InformationparticularsDaoImpl informationparticularsDao = new InformationparticularsDaoImpl();

//		String informationId = (String)request.getParameter("informationId");
//		System.out.println(informationId);
//新闻分类表
		int thisClassiID = 1;
		request.setAttribute("thisClassiID",thisClassiID);
		List<Informationclassification> inforClass = informationparticularsDao.getInforClassList();
		request.setAttribute("inforClass", inforClass);
//		System.out.println(inforClass);
		
		
//当前分类的新闻总条数
		int count = informationparticularsDao.getInforCount(thisClassiID);
		request.setAttribute("count", count);
		
		
		
//		新闻表
		List<Informationparticulars> informationparticularsList = informationparticularsDao.getInformationList(thisClassiID);
		request.setAttribute("informationparticularsList", informationparticularsList);

		
		request.getRequestDispatcher("article_list.jsp").forward(request, response);
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}

package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.PageTestDaoImpl;
import top.tangs.vitamin.dao.impl.ProductdetailsDaoImpl;
import top.tangs.vitamin.entity.Productclassification;
import top.tangs.vitamin.entity.Productdetails;

public class ProductsClassServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 产品表数据
		ProductdetailsDaoImpl ProductdetailsDao = new ProductdetailsDaoImpl();
		// 产品分类
		List<Productclassification> productClassificationList = ProductdetailsDao.getProductclassificationList();
		request.setAttribute("productClassificationList", productClassificationList);
		// 商品查询
		int productClassId = Integer.parseInt(request.getParameter("productClassId"));
		Object productClassIdString = (Object) productClassId;
		boolean idIsNull = productClassIdString.equals(null);

		// 查询商品总条数
//		PageTestDaoImpl pageTestDao = new PageTestDaoImpl();
//		int counts = pageTestDao.getProductdetailsPageCountList(productClassId);
//		request.setAttribute("counts", counts);
//		-----------------------------------------
//		分页部分
		//页码
		int productPage = 1;
		
//		每页条数
		int limitList = 4;
		//获取分类的总商品数量总数
		int count = ProductdetailsDao.getProductCount(productPage, productClassId);
		//可分页数
		int pageList = (count/limitList)+1;
//		System.out.println("当前页:"+productPage);
		request.setAttribute("productPage", productPage);
//		System.out.println(count);
//		System.out.println(pageList);
		request.setAttribute("pageList", pageList);
//		System.out.println(ProductdetailsList);
		
//   	-----------------------------------------
		
		
		
//商品查询		
		if (idIsNull) {
			// 查询所有商品
			List<Productdetails> ProductdetailsList = ProductdetailsDao.getFenYeProductdetailsList(productPage, productClassId,limitList);
			request.setAttribute("ProductdetailsList", ProductdetailsList);
		} else {
			// 分类查询商品
			List<Productdetails> ProductdetailsList = ProductdetailsDao.getFenYeProductdetailsList(productPage, productClassId,limitList);
			request.setAttribute("ProductdetailsList", ProductdetailsList);
			request.setAttribute("classId", productClassId);
		}
		request.getRequestDispatcher("productlist.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}

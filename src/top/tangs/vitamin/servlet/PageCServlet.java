package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.runtime.PageContextImpl;

import top.tangs.vitamin.dao.impl.PageTestDaoImpl;
import top.tangs.vitamin.dao.impl.ProductdetailsDaoImpl;
import top.tangs.vitamin.entity.Productclassification;
import top.tangs.vitamin.entity.Productdetails;

/**
 * Servlet implementation class PageCServlet
 */
public class PageCServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 产品表数据
		PageTestDaoImpl pageTestDao = new PageTestDaoImpl();
		ProductdetailsDaoImpl ProductdetailsDao = new ProductdetailsDaoImpl();
		// 产品分类
		List<Productclassification> productClassificationList = ProductdetailsDao.getProductclassificationList();
		request.setAttribute("productClassificationList", productClassificationList);
		// 商品查询
				//获取前台传来的参数
		int pageCount = Integer.parseInt(request.getParameter("pageCount"));//第x条
		int productClassId = Integer.parseInt(request.getParameter("productClassId"));//当前分页分类id
		request.setAttribute("pageCount", pageCount);
//		---------------------------------------------------------
		// 分页查询部分

		// 查询商品总条数
		int counts = pageTestDao.getProductdetailsPageCountList(productClassId);
		request.setAttribute("counts", counts);
//		System.out.println("商品总条数:"+counts);
//		// 分页查询商品
//		System.out.println("pageCount="+pageCount);
		// 商品查询
//		分页查询商品
		List<Productdetails> ProductdetailsList = pageTestDao.getProductdetailsPageList(pageCount,productClassId);
		request.setAttribute("ProductdetailsList", ProductdetailsList);//查询结果,带page
		request.setAttribute("classId", productClassId);//分类id,获取焦点用
//		System.out.println("ProductdetailsList="+ProductdetailsList);//查询数据
		int pageCounts = pageCount/4;
		request.setAttribute("pageCounts", pageCounts);
//		System.out.println("查询第条"+pageCount+"条到"+(pageCount+2));//第x条
//		System.out.println("当前分页分类id:"+productClassId);//当前分页分类id
//		System.out.println("当前分页页数:="+pageCounts);
		
//		---------------------------------------------------------

		
		
		request.getRequestDispatcher("productlist.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

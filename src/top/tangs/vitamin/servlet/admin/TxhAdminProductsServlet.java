package top.tangs.vitamin.servlet.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.tribes.group.Response;

import com.google.gson.Gson;

import top.tangs.vitamin.dao.impl.ProductdetailsDaoImpl;
import top.tangs.vitamin.entity.Productclassification;
import top.tangs.vitamin.util.BaseServlet;
import top.tangs.vitamin.util.Result;


/**   
*    
* 项目名称：vitamins   
* 类名称：TxhAdminProductsServlet   
* 类描述：   
* 创建人：Tang   
* 创建时间：2020年5月30日 下午8:31:49   
* @version        
*/
public class TxhAdminProductsServlet extends BaseServlet {

	/**
	 *方法名:getProductCates
	 *返回值类型:void
	 *方法描述:查询商品分类数据
 *地址:http://localhost:8080/vitamins/			
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 *创建时间:2020年5月30日 下午5:54:58
	 *作者Tang
	 *http://localhost:8080/vitamins/txhAdminProducts?reqName=getProductCates
	 */
	
	public void getProductCates(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
			
			
		ProductdetailsDaoImpl prodDaoImpl = new ProductdetailsDaoImpl();//创建实体类对象,
		List<Productclassification> pclist = prodDaoImpl.getProductclassificationList();//查到的所有产品分类的集合
		
		//发送Ajax请求,服务器一般(基本)都会返回json格式字符串
		
		request.setCharacterEncoding("UTF-8");//设置响应的编码格式
		PrintWriter out = response.getWriter();//out对象
		Gson gson = new Gson();
		String res =gson.toJson(pclist);//将pclist转化json格式字符串
		
		out.print(res);//响应数据
		
		out.flush();
		out.close();
	}
	/**
	 *方法名:getDelProductCates
	 *返回值类型:void
	 *方法描述:根据分类id删除分类
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 *创建时间:2020年5月31日 上午1:32:51
	 *作者Tang
	 *http://localhost:8080/vitamins/txhAdminProducts?reqName=getDelProductCates
	 */
	public void getDelProductCates(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		int classId = Integer.parseInt(request.getParameter("cid"));
		
		ProductdetailsDaoImpl prodDaoImpl = new ProductdetailsDaoImpl();//创建实体类对象,
		int n = prodDaoImpl.getDelPCclass(classId);//查到的所有产品分类的集合
		
		//发送Ajax请求,服务器一般(基本)都会返回json格式字符串
		
		request.setCharacterEncoding("UTF-8");//设置响应的编码格式
		PrintWriter out = response.getWriter();//out对象
		if(n>0) {
			out.print(Result.toClient(true,"删除成功",""));
		}else {
			out.print(Result.toClient(false, "删除失败",""));
		}
		out.flush();
		out.close();
	}
}

package top.tangs.vitamin.servlet.admin;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import top.tangs.vitamin.dao.impl.InformationparticularsDaoImpl;
import top.tangs.vitamin.entity.Informationparticulars;
import top.tangs.vitamin.util.BaseServlet;
import top.tangs.vitamin.util.Result;
public class AdminInformationparticularsServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * 查询所有资讯
	 * http://localhost:8080/vitamins/adminInformationparticulars?reqName=getinform
	 */
	public void getinform(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InformationparticularsDaoImpl infoDao = new InformationparticularsDaoImpl();
		List<Informationparticulars> infoList = infoDao.getInformaationList();
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		String res = gson.toJson(infoList);//转化json字符串
		out.print(res);
		out.flush();
		out.close();
	}
	/**
	 * 删除资讯
	 * http://localhost:8080/vitamins/adminInformationparticulars?reqName=delinform
	 */
	public void delinform(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int iId = Integer.parseInt(request.getParameter("infoId"));//接受前台传来的参数 
		InformationparticularsDaoImpl infoDao = new InformationparticularsDaoImpl();
		int n= infoDao.delproductClassificationId(iId);
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		if(n>0) {//删除成功
			out.print(Result.toClient(true, "删除成功",""));
		}else {//删除失败
			out.print(Result.toClient(false, "删除失败",""));
		}
		out.flush();
		out.close();
	}
}

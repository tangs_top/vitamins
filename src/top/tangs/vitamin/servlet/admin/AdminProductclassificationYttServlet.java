package top.tangs.vitamin.servlet.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import top.tangs.vitamin.dao.impl.ProductdetailsDaoImpl;
import top.tangs.vitamin.entity.Productclassification;
import top.tangs.vitamin.util.BaseServlet;
import top.tangs.vitamin.util.Result;

public class AdminProductclassificationYttServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * 查询所有产品分类
	 * http://localhost:8080/vitamins/adminProductclassificationYtt?reqName=getProduct
	 */
	public void getProduct(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductdetailsDaoImpl pdDao = new ProductdetailsDaoImpl();
		List<Productclassification> pdList = pdDao.getProductclassificationList();

		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		String res = gson.toJson(pdList);// 转化json字符串
		out.print(res);
		out.flush();
		out.close();
	}
	/**
	 * 删除产品分类
	 * http://localhost:8080/vitamins/adminProductclassificationYtt?reqName=delgetProduct
	 */
	public void delgetProduct(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int pId = Integer.parseInt(request.getParameter("cationId"));//接受前台传来的参数 
		ProductdetailsDaoImpl pdDao = new ProductdetailsDaoImpl();
		int n =  pdDao.delProductClassificationId(pId);//调用删除的方法
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		if(n>0) {//删除成功
			out.print(Result.toClient(true, "删除成功",""));
		}else {//删除失败
			out.print(Result.toClient(false, "删除失败",""));
		}
		out.flush();
		out.close();
	}
}

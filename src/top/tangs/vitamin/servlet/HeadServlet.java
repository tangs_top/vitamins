package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.CompanyDaoImpl;
import top.tangs.vitamin.dao.impl.SlideshowDaoImpl;
import top.tangs.vitamin.entity.Company;
import top.tangs.vitamin.entity.Slideshow;
public class HeadServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		轮播图数据
		SlideshowDaoImpl slideshowDao = new SlideshowDaoImpl();
		List<Slideshow> slideshowList = slideshowDao.getSlideshowList();
		request.setAttribute("slideshowList", slideshowList);
//		Company表数据
		CompanyDaoImpl  CompanyDao= new CompanyDaoImpl();
		Company Companys = new Company();
		Companys = CompanyDao.getCompanys();
		request.setAttribute("Companys", Companys);
		
		request.getRequestDispatcher("header.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}

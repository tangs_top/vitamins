package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.InformationparticularsDaoImpl;
import top.tangs.vitamin.entity.Informationclassification;
import top.tangs.vitamin.entity.Informationparticulars;
public class NewPage extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		新闻分类表
		InformationparticularsDaoImpl informationparticularsDao = new InformationparticularsDaoImpl();
		List<Informationclassification> inforClass = informationparticularsDao.getInforClassList();
		request.setAttribute("inforClass", inforClass);
		int thisClassiID = Integer.parseInt(request.getParameter("newClassId"));
		request.setAttribute("thisClassiID",thisClassiID);
		System.out.println(thisClassiID);
//		-----------------------------------------------------------------------------------
//		int classId = 1;//新闻分类id
//		System.out.println("当前新闻分类Id"+thisClassiID);
	//当前分类的新闻总条数
		int count = informationparticularsDao.getInforCount(thisClassiID);
		request.setAttribute("count", count);
//		System.out.println("当前分类的新闻总条数"+count);
	//当前查询第XX条数据
		int thisIndex = Integer.parseInt(request.getParameter("page"));//查询吓一条数据  
		request.setAttribute("thisIndex", thisIndex);
//		System.out.println("当前查询第"+thisIndex+"条数据");
		//上一篇
		Informationparticulars inforContentTop = informationparticularsDao.getInforThisCount(thisIndex-1, thisClassiID);
		request.setAttribute("inforContentTop", inforContentTop);
		//当前篇
		Informationparticulars inforContent = informationparticularsDao.getInforThisCount(thisIndex, thisClassiID);
		request.setAttribute("inforContent", inforContent);
		//下一篇
		Informationparticulars inforContentbottom = informationparticularsDao.getInforThisCount(thisIndex+1, thisClassiID);
		request.setAttribute("inforContentbottom", inforContentbottom);
		
		
//		System.out.println("查询结果"+inforContent);
//		-----------------------------------------------------------------------------------
		request.getRequestDispatcher("article_list_content.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

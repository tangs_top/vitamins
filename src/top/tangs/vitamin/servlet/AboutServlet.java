package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.DesignerDaoImpl;
import top.tangs.vitamin.entity.Designer;

/**
 * Servlet implementation class AboutServlet
 */
public class AboutServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		设计师表数据
		DesignerDaoImpl designerDao = new DesignerDaoImpl();
		List<Designer> designerList =  designerDao.getDesignerList();
		request.setAttribute("designerList", designerList);
		System.out.println(designerList);
		request.getRequestDispatcher("about.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

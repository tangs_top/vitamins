package top.tangs.vitamin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import top.tangs.vitamin.dao.impl.InformationparticularsDaoImpl;
import top.tangs.vitamin.dao.impl.ProductdetailsDaoImpl;
import top.tangs.vitamin.entity.Informationclassification;
import top.tangs.vitamin.entity.Informationparticulars;
import top.tangs.vitamin.entity.Productclassification;

/**
 * Servlet implementation class NewsDetailServlet
 */
public class NewsDetailServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
InformationparticularsDaoImpl informationparticularsDao = new InformationparticularsDaoImpl();
		
//		新闻分类表
		List<Informationclassification> inforClass = informationparticularsDao.getInforClassList();
		request.setAttribute("inforClass", inforClass);
//		System.out.println(inforClass);
		
		int nDid = Integer.parseInt(request.getParameter("informationId"));
		Object productClassIdString = (Object)nDid;
//		System.out.println(nDid);
		boolean idIsNull = productClassIdString == null;
		if(!idIsNull) {
			Informationparticulars inforContent = informationparticularsDao.getInforDetailList(nDid);
			request.setAttribute("inforContent", inforContent);
			System.out.println(inforContent);
			request.setAttribute("reDian", 0);
			request.getRequestDispatcher("article_list_content.jsp").forward(request, response);
		}else {
//			Informationparticulars inforContent = informationparticularsDao.getInforDetailList();
//			request.setAttribute("inforContent", inforContent);
//			System.out.println(inforContent);
		}	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}

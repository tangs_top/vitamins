package top.tangs.vitamin.entity;

public class Slideshow {
	private int slideshowId;
	private String slideshowImgName;
	public int getSlideshowId() {
		return slideshowId;
	}
	public void setSlideshowId(int slideshowId) {
		this.slideshowId = slideshowId;
	}
	public String getSlideshowImgName() {
		return slideshowImgName;
	}
	public void setSlideshowImgName(String slideshowImgName) {
		this.slideshowImgName = slideshowImgName;
	}
	public Slideshow(int slideshowId, String slideshowImgName) {
		super();
		this.slideshowId = slideshowId;
		this.slideshowImgName = slideshowImgName;
	}
	public Slideshow() {
		super();
	}
	@Override
	public String toString() {
		return "Slideshow [slideshowId=" + slideshowId + ", slideshowImgName=" + slideshowImgName + "]";
	}
	
}

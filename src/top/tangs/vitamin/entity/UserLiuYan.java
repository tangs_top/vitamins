package top.tangs.vitamin.entity;

public class UserLiuYan {

	private String username ;
	private String tel;
	private String email;
	private String content;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public UserLiuYan(String username, String tel, String email, String content) {
		super();
		this.username = username;
		this.tel = tel;
		this.email = email;
		this.content = content;
	}
	
	public UserLiuYan() {
		super();
	}
	@Override
	public String toString() {
		return "UserLiuYan [username=" + username + ", tel=" + tel + ", email=" + email + ", content=" + content + "]";
	}
	
}

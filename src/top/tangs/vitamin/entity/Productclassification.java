package top.tangs.vitamin.entity;

public class Productclassification {
	private int productClassificationId;
	private String productClassificationName;
	private String productClassificationImg;
	public int getProductClassificationId() {
		return productClassificationId;
	}
	public void setProductClassificationId(int productClassificationId) {
		this.productClassificationId = productClassificationId;
	}
	public String getProductClassificationName() {
		return productClassificationName;
	}
	public void setProductClassificationName(String productClassificationName) {
		this.productClassificationName = productClassificationName;
	}
	public String getProductClassificationImg() {
		return productClassificationImg;
	}
	public void setProductClassificationImg(String productClassificationImg) {
		this.productClassificationImg = productClassificationImg;
	}
	public Productclassification(int productClassificationId, String productClassificationName,
			String productClassificationImg) {
		super();
		this.productClassificationId = productClassificationId;
		this.productClassificationName = productClassificationName;
		this.productClassificationImg = productClassificationImg;
	}
	
	public Productclassification(int productClassificationId, String productClassificationName) {
		super();
		this.productClassificationId = productClassificationId;
		this.productClassificationName = productClassificationName;
	}
	public Productclassification() {
		super();
	}
	@Override
	public String toString() {
		return "Productclassification [productClassificationId=" + productClassificationId
				+ ", productClassificationName=" + productClassificationName + ", productClassificationImg="
				+ productClassificationImg + "]";
	}
	
	
}

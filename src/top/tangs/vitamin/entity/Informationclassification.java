package top.tangs.vitamin.entity;

public class Informationclassification {
	private int informationClassificationID;
	private String informationClassificationName;
	public int getInformationClassificationID() {
		return informationClassificationID;
	}
	public void setInformationClassificationID(int informationClassificationID) {
		this.informationClassificationID = informationClassificationID;
	}
	public String getInformationClassificationName() {
		return informationClassificationName;
	}
	public void setInformationClassificationName(String informationClassificationName) {
		this.informationClassificationName = informationClassificationName;
	}
	public Informationclassification(int informationClassificationID, String informationClassificationName) {
		super();
		this.informationClassificationID = informationClassificationID;
		this.informationClassificationName = informationClassificationName;
	}
	public Informationclassification() {
		super();
	}
	@Override
	public String toString() {
		return "Informationclassification [informationClassificationID=" + informationClassificationID
				+ ", informationClassificationName=" + informationClassificationName + "]";
	}
	
}	

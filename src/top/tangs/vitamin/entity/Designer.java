package top.tangs.vitamin.entity;

public class Designer {
	 private int designerId;
	 private String designerHeadPortraits;
	 private String designerName;
	 private String designerPosition;
	public int getDesignerId() {
		return designerId;
	}
	public void setDesignerId(int designerId) {
		this.designerId = designerId;
	}
	public String getDesignerHeadPortraits() {
		return designerHeadPortraits;
	}
	public void setDesignerHeadPortraits(String designerHeadPortraits) {
		this.designerHeadPortraits = designerHeadPortraits;
	}
	public String getDesignerName() {
		return designerName;
	}
	public void setDesignerName(String designerName) {
		this.designerName = designerName;
	}
	public String getDesignerPosition() {
		return designerPosition;
	}
	public void setDesignerPosition(String designerPosition) {
		this.designerPosition = designerPosition;
	}
	public Designer(int designerId, String designerHeadPortraits, String designerName, String designerPosition) {
		super();
		this.designerId = designerId;
		this.designerHeadPortraits = designerHeadPortraits;
		this.designerName = designerName;
		this.designerPosition = designerPosition;
	}
	public Designer() {
		super();
	}
	@Override
	public String toString() {
		return "Designer [designerId=" + designerId + ", designerHeadPortraits=" + designerHeadPortraits
				+ ", designerName=" + designerName + ", designerPosition=" + designerPosition + "]";
	}
}

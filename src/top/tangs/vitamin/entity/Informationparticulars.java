package top.tangs.vitamin.entity;

import java.util.Date;

public class Informationparticulars {
	int informationParticularsId;
	String informationParticularsTitle;
	String informationParticularsContent;
	String informationParticularsContentS;
	Date informationParticularsTime;
	
	int informationParticularsClassification;
	String informationClassificationName;
	public String getInformationParticularsContentS() {
		return informationParticularsContentS;
	}
	public void setInformationParticularsContentS(String informationParticularsContentS) {
		this.informationParticularsContentS = informationParticularsContentS;
	}
	public int getInformationParticularsId() {
		return informationParticularsId;
	}
	public void setInformationParticularsId(int informationParticularsId) {
		this.informationParticularsId = informationParticularsId;
	}
	public String getInformationParticularsTitle() {
		return informationParticularsTitle;
	}
	public void setInformationParticularsTitle(String informationParticularsTitle) {
		this.informationParticularsTitle = informationParticularsTitle;
	}
	public String getInformationParticularsContent() {
		return informationParticularsContent;
	}
	public void setInformationParticularsContent(String informationParticularsContent) {
		this.informationParticularsContent = informationParticularsContent;
	}
	public Date getInformationParticularsTime() {
		return informationParticularsTime;
	}
	public void setInformationParticularsTime(Date informationParticularsTime) {
		this.informationParticularsTime = informationParticularsTime;
	}
	public int getInformationClassificationID() {
		return informationParticularsClassification;
	}
	public void setInformationClassificationID(int informationClassificationID) {
		this.informationParticularsClassification = informationClassificationID;
	}
	public String getInformationClassificationName() {
		return informationClassificationName;
	}
	public void setInformationClassificationName(String informationClassificationName) {
		this.informationClassificationName = informationClassificationName;
	}
	public Informationparticulars(int informationParticularsId, String informationParticularsTitle,
			String informationParticularsContent, Date informationParticularsTime, int informationClassificationID,
			String informationClassificationName) {
		super();
		this.informationParticularsId = informationParticularsId;
		this.informationParticularsTitle = informationParticularsTitle;
		this.informationParticularsContent = informationParticularsContent;
		this.informationParticularsTime = informationParticularsTime;
		this.informationParticularsClassification = informationClassificationID;
		this.informationClassificationName = informationClassificationName;
	}
	public Informationparticulars(int informationParticularsId, String informationParticularsTitle,
			String informationParticularsContent, String informationParticularsContentS,
			int informationClassificationID) {
		super();
		this.informationParticularsId = informationParticularsId;
		this.informationParticularsTitle = informationParticularsTitle;
		this.informationParticularsContent = informationParticularsContent;
		this.informationParticularsContentS = informationParticularsContentS;
		this.informationParticularsClassification = informationClassificationID;
	}
	public Informationparticulars(int informationParticularsId, String informationParticularsTitle,
			String informationParticularsContentS, int informationParticularsClassification) {
		super();
		this.informationParticularsId = informationParticularsId;
		this.informationParticularsTitle = informationParticularsTitle;
		this.informationParticularsContentS = informationParticularsContentS;
		this.informationParticularsClassification = informationParticularsClassification;
	}
	
	public Informationparticulars(int informationParticularsId, String informationParticularsTitle,
			Date informationParticularsTime) {
		super();
		this.informationParticularsId = informationParticularsId;
		this.informationParticularsTitle = informationParticularsTitle;
		this.informationParticularsTime = informationParticularsTime;
	}
	public Informationparticulars() {
		super();
	}
	
	@Override
	public String toString() {
		return "Informationparticulars [informationParticularsId=" + informationParticularsId
				+ ", informationParticularsTitle=" + informationParticularsTitle + ", informationParticularsContent="
				+ informationParticularsContent + ", informationParticularsContentS=" + informationParticularsContentS
				+ ", informationParticularsTime=" + informationParticularsTime
				+ ", informationParticularsClassification=" + informationParticularsClassification
				+ ", informationClassificationName=" + informationClassificationName + "]";
	}
	public Informationparticulars(int informationParticularsId, String informationParticularsTitle,
			String informationParticularsContent) {
		super();
		this.informationParticularsId = informationParticularsId;
		this.informationParticularsTitle = informationParticularsTitle;
		this.informationParticularsContent = informationParticularsContent;
	}
	public Informationparticulars(int informationParticularsId, String informationParticularsTitle) {
		super();
		this.informationParticularsId = informationParticularsId;
		this.informationParticularsTitle = informationParticularsTitle;
	}

}

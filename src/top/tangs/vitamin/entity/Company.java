package top.tangs.vitamin.entity;

public class Company {
	private String companyName;
	private String companyWebsite;
	private String companyLogo;
	private String companyHotline;
	private String companyPhone;
	private String companyEmail;
	private String companyAddress;
	private String companyErWeiMa;
	private String companyIntroduce1;
	private String companyIntroduceImg1;
	private String companyIntroduce2;
	private String companyIntroduceImg2;
	private String companyCulture;
	private String companyCultureImg;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	public String getCompanyLogo() {
		return companyLogo;
	}
	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}
	public String getCompanyHotline() {
		return companyHotline;
	}
	public void setCompanyHotline(String companyHotline) {
		this.companyHotline = companyHotline;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getCompanyErWeiMa() {
		return companyErWeiMa;
	}
	public void setCompanyErWeiMa(String companyErWeiMa) {
		this.companyErWeiMa = companyErWeiMa;
	}
	public String getCompanyIntroduce1() {
		return companyIntroduce1;
	}
	public void setCompanyIntroduce1(String companyIntroduce1) {
		this.companyIntroduce1 = companyIntroduce1;
	}
	public String getCompanyIntroduceImg1() {
		return companyIntroduceImg1;
	}
	public void setCompanyIntroduceImg1(String companyIntroduceImg1) {
		this.companyIntroduceImg1 = companyIntroduceImg1;
	}
	public String getCompanyIntroduce2() {
		return companyIntroduce2;
	}
	public void setCompanyIntroduce2(String companyIntroduce2) {
		this.companyIntroduce2 = companyIntroduce2;
	}
	public String getCompanyIntroduceImg2() {
		return companyIntroduceImg2;
	}
	public void setCompanyIntroduceImg2(String companyIntroduceImg2) {
		this.companyIntroduceImg2 = companyIntroduceImg2;
	}
	public String getCompanyCulture() {
		return companyCulture;
	}
	public void setCompanyCulture(String companyCulture) {
		this.companyCulture = companyCulture;
	}
	public String getCompanyCultureImg() {
		return companyCultureImg;
	}
	public void setCompanyCultureImg(String companyCultureImg) {
		this.companyCultureImg = companyCultureImg;
	}
	public Company(String companyName, String companyWebsite, String companyLogo, String companyHotline,
			String companyPhone, String companyEmail, String companyAddress, String companyErWeiMa,
			String companyIntroduce1, String companyIntroduceImg1, String companyIntroduce2,
			String companyIntroduceImg2, String companyCulture, String companyCultureImg) {
		super();
		this.companyName = companyName;
		this.companyWebsite = companyWebsite;
		this.companyLogo = companyLogo;
		this.companyHotline = companyHotline;
		this.companyPhone = companyPhone;
		this.companyEmail = companyEmail;
		this.companyAddress = companyAddress;
		this.companyErWeiMa = companyErWeiMa;
		this.companyIntroduce1 = companyIntroduce1;
		this.companyIntroduceImg1 = companyIntroduceImg1;
		this.companyIntroduce2 = companyIntroduce2;
		this.companyIntroduceImg2 = companyIntroduceImg2;
		this.companyCulture = companyCulture;
		this.companyCultureImg = companyCultureImg;
	}
	public Company() {
		super();
	}
	@Override
	public String toString() {
		return "Company [companyName=" + companyName + ", companyWebsite=" + companyWebsite + ", companyLogo="
				+ companyLogo + ", companyHotline=" + companyHotline + ", companyPhone=" + companyPhone
				+ ", companyEmail=" + companyEmail + ", companyAddress=" + companyAddress + ", companyErWeiMa="
				+ companyErWeiMa + ", companyIntroduce1=" + companyIntroduce1 + ", companyIntroduceImg1="
				+ companyIntroduceImg1 + ", companyIntroduce2=" + companyIntroduce2 + ", companyIntroduceImg2="
				+ companyIntroduceImg2 + ", companyCulture=" + companyCulture + ", companyCultureImg="
				+ companyCultureImg + "]";
	}

	
}

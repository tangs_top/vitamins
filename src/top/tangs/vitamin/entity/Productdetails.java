package top.tangs.vitamin.entity;

public class Productdetails {
	private int productID;
	private String productName;
	private String productTexture;
	private int productPrice;
	private int productMarketPrice;
	private String productFlatPattern;
	private String productImg;

	private String productExplain;
	private String productDetailedDescription;
	private String productDrand;
	private String productVersion;
	private String productStyle;
	private String productKDorNot;
	private String productColorClassification;
	private String productInscribePositioning;
	private String productCustomDesign;
	private String productDesignElement;
	private int productTheCategory;
	private int page;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}



	public Productdetails() {
		super();
	}

	public String getProductImg() {
		return productImg;
	}
	
	public void setProductImg(String productImg) {
		this.productImg = productImg;
	}
	private String productClassificationName;
	private String productClassificationImg;

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public String getProductName() {
		return productName;
	}

	public Productdetails(int productID, String productFlatPattern) {
		super();
		this.productID = productID;
		this.productFlatPattern = productFlatPattern;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductTexture() {
		return productTexture;
	}

	public void setProductTexture(String productTexture) {
		this.productTexture = productTexture;
	}

	public int getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(int productPrice) {
		this.productPrice = productPrice;
	}

	public int getProductMarketPrice() {
		return productMarketPrice;
	}

	public void setProductMarketPrice(int productMarketPrice) {
		this.productMarketPrice = productMarketPrice;
	}

	public String getProductFlatPattern() {
		return productFlatPattern;
	}

	public void setProductFlatPattern(String productFlatPattern) {
		this.productFlatPattern = productFlatPattern;
	}

	public String getProductExplain() {
		return productExplain;
	}

	public void setProductExplain(String productExplain) {
		this.productExplain = productExplain;
	}

	public String getProductDetailedDescription() {
		return productDetailedDescription;
	}

	public void setProductDetailedDescription(String productDetailedDescription) {
		this.productDetailedDescription = productDetailedDescription;
	}

	public String getProductDrand() {
		return productDrand;
	}

	public void setProductDrand(String productDrand) {
		this.productDrand = productDrand;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public String getProductStyle() {
		return productStyle;
	}

	public void setProductStyle(String productStyle) {
		this.productStyle = productStyle;
	}

	public String getProductKDorNot() {
		return productKDorNot;
	}

	public void setProductKDorNot(String productKDorNot) {
		this.productKDorNot = productKDorNot;
	}

	public String getProductColorClassification() {
		return productColorClassification;
	}

	public void setProductColorClassification(String productColorClassification) {
		this.productColorClassification = productColorClassification;
	}

	public String getProductInscribePositioning() {
		return productInscribePositioning;
	}

	public void setProductInscribePositioning(String productInscribePositioning) {
		this.productInscribePositioning = productInscribePositioning;
	}

	public String getProductCustomDesign() {
		return productCustomDesign;
	}

	public void setProductCustomDesign(String productCustomDesign) {
		this.productCustomDesign = productCustomDesign;
	}

	public String getProductDesignElement() {
		return productDesignElement;
	}

	public void setProductDesignElement(String productDesignElement) {
		this.productDesignElement = productDesignElement;
	}

	public int getProductTheCategory() {
		return productTheCategory;
	}

	public void setProductTheCategory(int productTheCategory) {
		this.productTheCategory = productTheCategory;
	}

	public String getProductClassificationName() {
		return productClassificationName;
	}

	public void setProductClassificationName(String productClassificationName) {
		this.productClassificationName = productClassificationName;
	}

	public String getProductClassificationImg() {
		return productClassificationImg;
	}

	public Productdetails(int productTheCategory) {
		super();
		this.productTheCategory = productTheCategory;
	}

	public void setProductClassificationImg(String productClassificationImg) {
		this.productClassificationImg = productClassificationImg;
	}


	public Productdetails(int productID, String productName, String productFlatPattern) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.productFlatPattern = productFlatPattern;
	}

	

	public Productdetails(int productID, String productName, String productTexture, int productPrice,
			int productMarketPrice, String productFlatPattern,String productImg, String productExplain, String productDetailedDescription,
			String productDrand, String productVersion, String productStyle, String productKDorNot,
			String productColorClassification, String productInscribePositioning, String productCustomDesign,
			String productDesignElement, int productTheCategory, String productClassificationName) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.productTexture = productTexture;
		this.productPrice = productPrice;
		this.productMarketPrice = productMarketPrice;
		this.productFlatPattern = productFlatPattern;
		this.productImg = productImg;
		this.productExplain = productExplain;
		this.productDetailedDescription = productDetailedDescription;
		this.productDrand = productDrand;
		this.productVersion = productVersion;
		this.productStyle = productStyle;
		this.productKDorNot = productKDorNot;
		this.productColorClassification = productColorClassification;
		this.productInscribePositioning = productInscribePositioning;
		this.productCustomDesign = productCustomDesign;
		this.productDesignElement = productDesignElement;
		this.productTheCategory = productTheCategory;
		this.productClassificationName = productClassificationName;
	}


	public Productdetails(int productID, String productName, String productTexture, int productPrice,
			int productMarketPrice, String productFlatPattern, String productImg, String productExplain,
			String productDetailedDescription, String productDrand, String productVersion, String productStyle,
			String productKDorNot, String productColorClassification, String productInscribePositioning,
			String productCustomDesign, String productDesignElement, int productTheCategory, int page,
			String productClassificationName, String productClassificationImg) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.productTexture = productTexture;
		this.productPrice = productPrice;
		this.productMarketPrice = productMarketPrice;
		this.productFlatPattern = productFlatPattern;
		this.productImg = productImg;
		this.productExplain = productExplain;
		this.productDetailedDescription = productDetailedDescription;
		this.productDrand = productDrand;
		this.productVersion = productVersion;
		this.productStyle = productStyle;
		this.productKDorNot = productKDorNot;
		this.productColorClassification = productColorClassification;
		this.productInscribePositioning = productInscribePositioning;
		this.productCustomDesign = productCustomDesign;
		this.productDesignElement = productDesignElement;
		this.productTheCategory = productTheCategory;
		this.page = page;
		this.productClassificationName = productClassificationName;
		this.productClassificationImg = productClassificationImg;
	}

	public Productdetails(int productID, String productName, String productFlatPattern, int page) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.productFlatPattern = productFlatPattern;
		this.page = page;
	}

	@Override
	public String toString() {
		return "Productdetails [productID=" + productID + ", productName=" + productName + ", productTexture="
				+ productTexture + ", productPrice=" + productPrice + ", productMarketPrice=" + productMarketPrice
				+ ", productFlatPattern=" + productFlatPattern + ", productImg=" + productImg + ", productExplain="
				+ productExplain + ", productDetailedDescription=" + productDetailedDescription + ", productDrand="
				+ productDrand + ", productVersion=" + productVersion + ", productStyle=" + productStyle
				+ ", productKDorNot=" + productKDorNot + ", productColorClassification=" + productColorClassification
				+ ", productInscribePositioning=" + productInscribePositioning + ", productCustomDesign="
				+ productCustomDesign + ", productDesignElement=" + productDesignElement + ", productTheCategory="
				+ productTheCategory + ", page=" + page + ", productClassificationName=" + productClassificationName
				+ ", productClassificationImg=" + productClassificationImg + "]";
	}

	


}

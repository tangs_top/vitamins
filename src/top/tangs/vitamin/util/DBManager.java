package top.tangs.vitamin.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager {
//	   private final static String DRIVER="com.mysql.jdbc.Driver";
//       private final static String URL="jdbc:mysql://localhost:3306/vitamin";
//       private final static String USER="root";
//       private final static String PWD="admin";
	
	
       private final static String DRIVER="com.mysql.jdbc.Driver";
       private final static String URL="jdbc:mysql://182.92.0.40:3306/vitamin?useUnicode=true&characterEncoding=utf8";
       private final static String USER="vitamin";
       private final static String PWD="vitaminvitamin";
       public static Connection getConnection() {
    	   try {
			Class.forName(DRIVER);
			return DriverManager.getConnection(URL,USER,PWD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
       }
       public static int executeUpdate(String sql) {
    	   Connection conn=null;
    	   Statement st=null;
    	   try {
    		   conn =getConnection();
    		   st = conn.createStatement();
    		   return st.executeUpdate(sql);
    	   } catch(SQLException e) {
    		   e.printStackTrace();
    		   return 0;
    	   } finally {
    		   closeAll(conn,st,null);
    	   }
       }
       public static void closeAll(Connection conn,Statement st,ResultSet rs) {
    	   try {
    		   if(rs!=null) {
    			   rs.close();
    		   }
    		   if(st !=null) {
    			   st.close();
    		   }
    		   if(conn!=null) {
    			   conn.close();
    		   }
    	   }
    	   catch(SQLException e) {
			   e.printStackTrace();
		   }
       }
}
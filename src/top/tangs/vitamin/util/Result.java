package top.tangs.vitamin.util;

import com.google.gson.Gson;

/** * 处理结果 * @author Administrator * */
public class Result {
	public boolean success;
	public String message;
	public String remark;

	public static String toClient(boolean suc, Object msg) {
		Result r = new Result();
		r.success = suc;
		r.message = msg.toString();
		r.remark = "";
		Gson t = new Gson();
		return t.toJson(r);
	}
//加了static的方法,可以直接通过"类名.方法名()"来进行调用,Result.toCline(true,"成功","")
	public static String toClient(boolean suc, Object msg, String remark) {
		Result r = new Result();
		r.success = suc;
		r.message = msg.toString();
		r.remark = remark;
		Gson t = new Gson();
		return t.toJson(r);
	}
}
